﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace ColorDetector.Code
{
    class CSVReader
    {
        public static List<ColorModel> ReadCSV(string pathToFile) {

            string[] csv = File.ReadAllLines(pathToFile, Encoding.Default);
            List<ColorModel> colors = new List<ColorModel>();
            foreach (string s1 in csv) {
                var name = s1.Split(';')[0];
                var rgb = s1.Split(';')[1].Split(',');
                Color color = new Color();
                if (rgb.Length >= 3) {
                    color = Color.FromArgb(int.Parse(rgb[0]), int.Parse(rgb[1]), int.Parse(rgb[2]));
                }
                colors.Add(new ColorModel(){Value = color,Key = name});
            }

            return colors;
        }
    }
}
