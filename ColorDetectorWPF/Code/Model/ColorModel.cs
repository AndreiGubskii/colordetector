﻿using Color = System.Drawing.Color;

namespace ColorDetector.Code
{
    class ColorModel
    {
        public string Key { get; set; }
        public Color Value { get; set; }
    }
}
