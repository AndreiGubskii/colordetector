﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorDetectorWPF.Code.Model
{
    class Settings
    {
        public int quality { get; set; }
        public int colorCount { get; set; }

        public Settings() {
        }

        public Settings(int quality, int colorCount) {
            this.quality = quality;
            this.colorCount = colorCount;
        }
    }
}
