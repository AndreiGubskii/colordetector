﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using ColorDetector.Code;
using ColorThiefDotNet;
using TriggersTools.Asciify;
using TriggersTools.Asciify.ColorMine.Comparisons;
using TriggersTools.Asciify.ColorMine.Converters;
using Image = System.Drawing.Image;

//--< using >-- 
using Microsoft.Win32; //FileDialog 
using WinForms = System.Windows.Forms; //FolderDialog 
using System.IO; //Folder, Directory 
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using ColorDetectorWPF.Code.Model;

//Debug.WriteLine 
//--</ using >-- 
namespace ColorDetectorWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static List<ColorModel> colors = new List<ColorModel>();
        private static string path = "";
        private static Dictionary<string, List<QuantizedColor>> imagePalette = new Dictionary<string, List<QuantizedColor>>();
        private static int countFiles;
        private string logs = "";
        private bool isSaveResult = false;
        private BackgroundWorker bw;
        private Settings settings;
        List<string> images = new List<string>();
        public MainWindow()
        {
            InitializeComponent();

            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += StartWork;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            settings = Settings();
        }


        //Нажатие на кнопку начать
        private void BtnStartClick(object sender, RoutedEventArgs e)
        {
            colors = CSVReader.ReadCSV(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Data\\colors_db.csv");

            if (Directory.Exists(path))
            {
                // Получаем все изображения из указанной директории
                images = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).ToList();
                countFiles = images.Count;
                pbStatus.Maximum = countFiles;
                startBtn.IsEnabled = false;
                bw.RunWorkerAsync();
            }
            else
            {
                Log("Вы не выбрали папку с изображениями! \n", "error");
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Log($"Произошел сбой в работе: {e.Error.Message}");
            }
            else
            {
                Log($"Работа завершена успешно!\n");
                if (isSaveResult)
                {
                    Log($"Файл с результатом сохранен: {path}\n");
                }
                if (!string.IsNullOrEmpty(logs))
                {
                    Log(logs, "error");
                }
            }
            imagePalette = new Dictionary<string, List<QuantizedColor>>();
            logs = "";
            startBtn.IsEnabled = true;
            isSaveResult = false;
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (logText.Text != "")
            {
                logText.Text = "";
            }
            pbStatus.Value = e.ProgressPercentage;
            progressText.Text = (string)e.UserState;
        }
        public void StartWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            CieDe2000Comparison comparison = new CieDe2000Comparison();
            ParallelLoopResult result = new ParallelLoopResult();

            int counter = 0;
            result = Parallel.ForEach<string>(
                images,
                new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount / 2 },
            (image) =>
            {
                counter++;
                worker.ReportProgress(counter, $"Обработанно {counter} из {countFiles}.");
                CreatImagePallette(image);
            }
            );

            if (result.IsCompleted)
            {
                List<string> resultLines = new List<string>();
                foreach (var image in imagePalette)
                {
                    Dictionary<string, double> ar = new Dictionary<string, double>();
                    List<string> resultColors = new List<string>();
                    string resultLine = image.Key;
                    foreach (QuantizedColor color in image.Value)
                    {
                        ar = new Dictionary<string, double>();
                        ColorLab color1 = LabConverter.ToLab(new ColorRgb(color.Color.R, color.Color.G, color.Color.B));

                        foreach (var c in colors)
                        {
                            ColorLab color2 = LabConverter.ToLab(new ColorRgb(c.Value.R, c.Value.G, c.Value.B));

                            double v = comparison.Compare(color2, color1);
                            if (ar.ContainsKey(c.Key))
                            {
                                if (v < ar[c.Key])
                                {
                                    ar[c.Key] = v;
                                }
                            }
                            else
                            {
                                ar.Add(c.Key, v);
                            }

                        }
                        if (!resultColors.Contains(ar.Aggregate(((l, r) => l.Value < r.Value ? l : r)).Key))
                        {
                            resultColors.Add(ar.Aggregate(((l, r) => l.Value < r.Value ? l : r)).Key);
                        }
                    }

                    foreach (string resultColor in resultColors)
                    {
                        resultLine += ", " + resultColor;
                    }
                    resultLines.Add(resultLine);
                }
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.FileName = "ПалитраИзображений.txt";
                saveFileDialog.Filter = "Text File | *.txt";

                if (saveFileDialog.ShowDialog() == true)
                {
                    isSaveResult = true;
                    File.WriteAllLines(saveFileDialog.FileName, resultLines, Encoding.Default);
                }

            }
        }

        //Выбрать папку с фото
        private void BtnSelectFolderClick(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog();
            folderDialog.ShowNewFolderButton = false;
            folderDialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            WinForms.DialogResult result = folderDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                //----< Selected Folder >---- 
                //< Selected Path > 
                String sPath = folderDialog.SelectedPath;
                tbxFolder.Text = sPath;
                path = sPath;
                //</ Selected Path > 
            }
        }


        private void CreatImagePallette(string image)
        {
            try
            {
                var colorThief = new ColorThief();
                var bitmap = (Bitmap)Image.FromFile(image);

                // Получаем цвета изображения
                List<QuantizedColor> result = colorThief.GetPalette(bitmap, settings.colorCount, settings.quality);
                bitmap.Dispose();
                // Берем наименование изображения
                Regex _regex = new Regex(@"[\D:\\][\S*||\s]*\\([\S*||\s*]*)\.\D*$");
                var match = _regex.Match(image);
                if (match.Success)
                {
                    lock (imagePalette)
                    {
                        imagePalette.Add(match.Groups[1].Value, result);
                    }
                }
                else
                {
                    logs += $"Не удалось обработать файл: {image} . Неправельное наименование файла\n";
                }
            }
            catch (Exception e)
            {
                logs += $"\nНе удалось обработать файл: {image} : {e.Message}\n";
            }
        }

        private void Log(string message, string type = "default")
        {
            switch (type)
            {
                case "error":
                    logText.Foreground = new SolidColorBrush(Colors.Red);
                    logText.AppendText(message);
                    break;
                default:
                    logText.Foreground = new SolidColorBrush(Colors.Black);
                    logText.AppendText(message);
                    break;
            }
        }

        //Настройки приложения
        private Settings Settings()
        {
            Settings appSettings = new Settings(10, 3);
            XmlDocument doc = new XmlDocument();
            doc.Load(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Data\\settings.xml");
            string quality = doc.DocumentElement.SelectSingleNode("settings/quality").InnerText;
            string colorCount = doc.DocumentElement.SelectSingleNode("settings/colorCount").InnerText;

            if (!string.IsNullOrEmpty(quality) && !string.IsNullOrEmpty(colorCount)) {
                bool q_success = Int32.TryParse(quality, out int q);
                bool c_success = Int32.TryParse(colorCount, out int c);

                if (q_success && c_success) {
                    appSettings = new Settings(q, c);
                }
            }


            return appSettings;
        }
    }
}

