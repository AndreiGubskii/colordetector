﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ColorDetector.Code;
using ColorThiefDotNet;
using TriggersTools.Asciify;
using TriggersTools.Asciify.ColorMine.Comparisons;
using TriggersTools.Asciify.ColorMine.Converters;
using Image = System.Drawing.Image;


namespace ColorDetector {
    class Program {
        private static List<ColorModel> colors = new List<ColorModel>();
        private static string path = "C:\\pic_test";
        private static Dictionary<string,List<QuantizedColor>> imagePalette = new Dictionary<string, List<QuantizedColor>>();
        private static int countFiles = 0;
        static void Main(string[] args) {
            Console.WriteLine("Color Detector начал работу!");
            colors = CSVReader.ReadCSV(@"c:\Цвета1000-ok.csv");

            // Получаем все изображения из указанной директории
            List<string> images = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).ToList();
            countFiles = images.Count;
            CieDe2000Comparison comparison = new CieDe2000Comparison();
            ParallelLoopResult result = new ParallelLoopResult();
            result = Parallel.ForEach<string>(
                images,
                new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount / 2 },
                CreatImagePallette);
            
            
            if (result.IsCompleted) {
                List<string> resultLines = new List<string>();
                foreach (var image in imagePalette)
                {
                    Dictionary<string, double> ar = new Dictionary<string, double>();
                    List<string> resultColors = new List<string>();
                    string resultLine = image.Key;
                    foreach (QuantizedColor color in image.Value)
                    {
                        ar = new Dictionary<string, double>();
                        ColorLab color1 = LabConverter.ToLab(new ColorRgb(color.Color.R, color.Color.G, color.Color.B));

                        foreach (var c in colors)
                        {
                            ColorLab color2 = LabConverter.ToLab(new ColorRgb(c.Value.R, c.Value.G, c.Value.B));

                            double v = comparison.Compare(color2, color1);
                            if (ar.ContainsKey(c.Key)) {
                                if (v < ar[c.Key]) {
                                    ar[c.Key] = v;
                                }
                            }
                            else {
                                ar.Add(c.Key, v);
                            }
                            
                        }
                        if (!resultColors.Contains(ar.Aggregate(((l, r) => l.Value < r.Value ? l : r)).Key))
                        {
                            resultColors.Add(ar.Aggregate(((l, r) => l.Value < r.Value ? l : r)).Key);
                        }
                    }
                    
                    foreach (string resultColor in resultColors) {
                        resultLine += ", " + resultColor;
                    }
                    resultLines.Add(resultLine);
                }
                File.WriteAllLines("C:\\Users\\andre\\Desktop\\result.txt", resultLines, Encoding.Default);
                Console.WriteLine("\nРабота завершена! \nФайл с результатом сохранен по пути {0}", "C:\\Users\\andre\\Desktop\\result.txt");
            }
            
            
            Console.ReadLine();
        }

        private static int i = 1;
        
        public static void CreatImagePallette(string image) {
            try
            {
                var colorThief = new ColorThief();
                var bitmap = (Bitmap)Image.FromFile(image);

                // Получаем цвета изображения
                List<QuantizedColor> result = colorThief.GetPalette(bitmap, 3,100);
                bitmap.Dispose();
                // Берем наименование изображения
                Regex _regex = new Regex(@"[\D:\\][\S*||\s]*\\([\S*||\s*]*)\.\D*$");
                var match = _regex.Match(image);
                if (match.Success) {
                    lock (imagePalette) {
                        imagePalette.Add(match.Groups[1].Value, result);
                    }
                    Console.Write("\rИдет обработка изображений, ожидайте... {0}/{1} ", i, countFiles);
                    i++;
                }
                else {
                    Console.WriteLine("Не удалось обработать файл: {0} . Неправельное наименование файла", image);
                    Console.ResetColor();
                    Console.WriteLine("\nColor Detector продолжит работу!");
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n" + e.Message);
                Console.WriteLine("Не удалось обработать файл: {0}",image);
                Console.ResetColor();
                Console.WriteLine("\nColor Detector продолжит работу!");
            }
        }
    }
}